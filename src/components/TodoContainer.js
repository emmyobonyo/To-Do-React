import React, { Component } from 'react'

export class ToDoContainer extends Component {
  constructor(props) {
    super(props)

    this.state = {
       todos: [
         {
           id: 1,
           title: 'This is the first task',
           completed: false,
         },
         {
           id: 2,
           title: 'This is the second task',
           //? Adding State.
         }
       ]
    }
  }
  
  render() {
    return (
      <React.Fragment>
        <h1>Hello from Create React App</h1>
        <p>I'm a React Component!</p>
      </React.Fragment>
    )
  }
}

export default ToDoContainer

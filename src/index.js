import React from 'react'
import { StrictMode } from 'react'
import ReactDom from 'react-dom'
import ToDoContainer from './components/TodoContainer'

ReactDom.render(
<StrictMode>
  <ToDoContainer/>
</StrictMode>,
document.getElementById('root'))